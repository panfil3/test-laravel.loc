<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/categories/{category}/{news}', 'NewsController@show')->name('news');
Route::get('/categories/{category}', 'NewsController@showByCategory')->name('news-by-category');
Route::get('/search', 'NewsController@getSearchResult')->name('search-result');
Route::get('/{user_id}/favorites', 'FavoritesController@show')->name('favorites');
Route::post('/{user_id}/{news_id}/favorites', 'FavoritesController@addToFavorites')->name('add-to-favorites');
Route::post('/{user_id}/{category_id}/subscribe', 'SubscriptionsController@subscribe')->name('subscribe');

Route::get('/mail', 'SubscriptionsController@show');
