<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    protected $fillable = [
        'id',
        'title',
        'description',
        'views',
    ];

    public static function getNews($catSlug, $newsSlug, $params = [])
    {
        $query = DB::table('news');
        $query->select([
            'news.id as nid',
            'news.slug as news_slug',
            'news.title as news_title',
            'news.title as news_title',
            'news.description as news_description',
            'news.views as news_views',
            'news.created_at as news_created_at',
            'categories.id as cat_id',
            'categories.name as cat_name',
            'categories.slug as cat_slug',
            'news_categories.category_id',
            'news_categories.news_id',
            ]);
        $query->leftJoin('news_categories', function ($join) {
            $join->on('news_categories.news_id', '=', 'news.id');
        });

        $query->leftJoin('categories', function ($join) {
            $join->on('news_categories.category_id', '=', 'categories.id');
        });

        //$query->where('categories.slug', '=', $catSlug);
        $query->where('news.slug', '=', $newsSlug);

        $news = $query->first();

        return $news;
    }

    public static function getList($catSlug = '', $params = [], $orderBy = 'views', $orderDirection = 'DESC') {

        $orderByQ = 'news.views';
        if ($orderBy == 'views') {
            $orderByQ = 'news.views';
        } elseif ($orderBy == 'categories') {
            $orderByQ = 'categories.name';
        }

        $orderDirectionQ = 'desc';
        if ($orderDirection == 'DESC') {
            $orderDirectionQ = 'desc';
        } elseif ($orderDirection == 'ASC') {
            $orderDirectionQ = 'asc';
        }

        $query = DB::table('news');
        $query->select([
            'news.*',
            'categories.slug as cat_slug',
            'categories.name as category',
            'news_categories.category_id',
            'news_categories.news_id',
            DB::raw('if(ISNULL(favorites),0,1) as favorites')
        ]);

        $query->leftJoin('news_categories', 'news_categories.news_id', '=','news.id');
        $query->leftJoin('categories', 'news_categories.category_id','=', 'categories.id');

        $f_q = DB::table('favorites');
        $f_q->select([
            'favorites.id as favorites',
            'favorites.news_id',
            'favorites.user_id',
            'favorites.created_at'
        ]);
        $f_q->leftJoin('news', 'news.id','=','favorites.news_id');
        $f_q->groupBy('favorites.id');

        $query->mergeBindings($f_q);
        $query->leftJoin(DB::raw('(' . $f_q->toSql() . ') AS favorites'), function ($join) {
            $join->on('favorites.news_id', '=', 'news.id');
        });

        if(!empty($catSlug)) {
            $query->where('categories.slug', '=', $catSlug);
        }

        if (isset($params['query'])) {

            $query->where(function ($query) use ($params) {

                $query_low = '%' . strtolower($params['query']) . '%';

                $query->whereRaw('LOWER(news.title) LIKE ?', [$query_low]);
                $query->orWhereRaw('LOWER(news.description) LIKE ?', [$query_low]);

            });
        }

        $query->orderBy($orderByQ, $orderDirectionQ);
        $news = $query->paginate(5);

        return $news;
    }
}
