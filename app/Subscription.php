<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Subscription extends Model
{
    use SoftDeletes;

    protected $table = 'subscriptions';

    protected $fillable = [
        'category_id',
        'user_id'
    ];

    public static function getInfo($user_id, $category_id)
    {
        $query = DB::table('subscriptions');
        $query->select([
            'subscriptions.*',
            'users.name as user_name',
            'users.email',
            'categories.name as cat_name'
        ]);
        $query->leftJoin('users','users.id','=','subscriptions.user_id');
        $query->leftJoin('categories','categories.id','=','subscriptions.category_id');

        $query->where('subscriptions.user_id',$user_id);
        $query->where('subscriptions.category_id',$category_id);

        return $query->first();
    }
}
