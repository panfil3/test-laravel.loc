<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News_category extends Model
{
    use SoftDeletes;

    protected $table = 'news_categories';

    protected $fillable = [
        'category_id',
        'news_id',
    ];
}
