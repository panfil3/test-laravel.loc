<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Favorite extends Model
{
    use SoftDeletes;

    protected $table = 'favorites';

    protected $fillable = [
        'news_id',
        'user_id'
    ];

    public static function getList($user_id, $params = [])
    {
        $query = DB::table('news');
        $query->select([
            'news.*',
            'categories.slug as cat_slug',
            'categories.name as category',
            'news_categories.category_id',
            DB::raw('if(ISNULL(favorites),0,1) as favorites')
        ]);
        $query->leftJoin('news_categories', 'news_categories.news_id', '=','news.id');
        $query->leftJoin('categories', 'news_categories.category_id','=', 'categories.id');

        $f_q = DB::table('favorites');
        $f_q->select([
            'favorites.id as favorites',
            //DB::raw('GROUP_CONCAT(favorites.id) as favorites'),
            'favorites.news_id',
            'favorites.user_id',
            'favorites.created_at'
        ]);
        $f_q->leftJoin('news', 'news.id','=','favorites.news_id');
        $f_q->groupBy('favorites.id');

        $query->mergeBindings($f_q);
        $query->leftJoin(DB::raw('(' . $f_q->toSql() . ') AS favorites'), function ($join) {
            $join->on('favorites.news_id', '=', 'news.id');
        });

        $query->where('favorites.user_id', '=', $user_id);
        $query->orderBy('favorites.created_at', 'DESC');

        $news = $query->paginate(5);

        return $news;
    }
}
