<?php

namespace App\Http\Controllers;


use App\Favorite;
use App\Http\Requests\AddToFavoritesRequest;
use App\Http\Requests\FavoritesRequest;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    public function show($user_id, FavoritesRequest $request)
    {
        $favorites = Favorite::getList($user_id);

        return view('favorites', ['news' => $favorites]);
    }

    public function addToFavorites($user_id, $news_id, AddToFavoritesRequest $request)
    {
        $params = [
            'news_id' => $news_id,
            'user_id' => $user_id
        ];

        Favorite::create($params);

        return redirect()->back();
    }
}
