<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Http\Requests\NewsByCategoryRequest;
use App\Http\Requests\SearchResultRequest;
use App\News;
use App\Subscription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function show($categorySlug, $newsSlug, NewsRequest $request)
    {
        DB::table('news')->where('slug', $newsSlug)->increment('views');
        $news = News::getNews($categorySlug, $newsSlug);

        return view('news', [
            'news' => $news
        ]);
    }

    public function showByCategory($categorySlug, NewsByCategoryRequest $request)
    {
        $query = $request->input('sort_dir');

        $sortBy = 'views';
        $sortDir = 'DESC';
        $sortDirLink = 'asc';

        if (!empty($query)){
            if ($query == 'desc'){
                $sortDir = 'DESC';
                $sortDirLink = 'asc';
            }
            elseif($query == 'asc') {
                $sortDir = 'ASC';
                $sortDirLink = 'desc';
            }
        }

        $news = News::getList($categorySlug, [], $sortBy, $sortDir);
        $category_id = DB::table('categories')->where('slug',$categorySlug)->value('id');

        $subscription = Subscription::getInfo(Auth::user()->id, $category_id);

        return view('news_list', [
            'cat_slug' => $categorySlug,
            'cat_id' => $category_id,
            'subscription' => $subscription,
            'news' => $news,
            'sortDirLink' => $sortDirLink
        ]);
    }

    public function getSearchResult(SearchResultRequest $request)
    {
        $sortBy = 'views';
        $search_data = $request->input('query');
        $input = $request->input('sort_by');
        $query = $request->input('sort_dir');

        $params_processed = null;
        if (!empty($search_data))
            $params_processed = $this::clean($search_data);

        if (!empty($input)){
            if ($input == 'categories') {
                $sortBy = 'categories';
            }
            elseif($input == 'views') {
                $sortBy = 'views';
            }
        }

        $sortDir = 'DESC';
        $sortDirLink = 'asc';

        if (!empty($query)){
            if ($query == 'desc'){
                $sortDir = 'DESC';
                $sortDirLink = 'asc';
            }
            elseif($query == 'asc') {
                $sortDir = 'ASC';
                $sortDirLink = 'desc';
            }
        }

        $news = News::getList('', ['query' => $params_processed,], $sortBy, $sortDir);

        return view('search_result_list', [
            'news' => $news,
            'params' => ['sortBy' => $sortBy],
            'sortDirLink' => $sortDirLink
        ]);
    }

    public static function clean($string) {
        $string = preg_replace('/[^A-Za-zА-Яа-я0-9\-]/u', ' ', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
