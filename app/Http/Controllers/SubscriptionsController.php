<?php

namespace App\Http\Controllers;


use App\Http\Requests\SubscribeRequest;
use App\Subscription;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SubscriptionsController extends Controller
{
    public function subscribe($user_id, $category_id, SubscribeRequest $request)
    {
        $params = [
            'user_id' => $user_id,
            'category_id' => $category_id
        ];
        Subscription::create($params);

        /*$subscription = Subscription::getInfo($user_id, $category_id);

        $data = [
            'user' => $subscription->user_name,
            'email' => $subscription->email,
            'category' => $subscription->cat_name
        ];

        Mail::send('emails.subscription', ['data' => $data], function ($m) use ($data) {
            $m->to($data['email'], $data['user'])->subject('Subscription');
        });*/

        return redirect()->back()->with('subscribed', 'Subscribed');
    }
}
