<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests\NewsSortingRequest;
use App\News;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(NewsSortingRequest $request)
    {
        $sortBy = 'views';
        $input = $request->input('sort_by');
        $query = $request->input('sort_dir');

        if (!empty($input)){
            if ($input == 'categories') {
                $sortBy = 'categories';
            }
            elseif($input == 'views') {
                $sortBy = 'views';
            }
        }

        $sortDir = 'DESC';
        $sortDirLink = 'asc';

        if (!empty($query)){
            if ($query == 'desc'){
                $sortDir = 'DESC';
                $sortDirLink = 'asc';
            }
            elseif($query == 'asc') {
                $sortDir = 'ASC';
                $sortDirLink = 'desc';
            }
        }

        $news = News::getList('', [], $sortBy, $sortDir);

        return view('home', [
            'news' => $news,
            'params' => ['sortBy' => $sortBy],
            'sortDirLink' => $sortDirLink
            ]);
    }
}
