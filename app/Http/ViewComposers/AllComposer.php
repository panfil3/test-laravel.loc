<?php

namespace App\Http\ViewComposers;

use App\Category;
use Illuminate\Contracts\View\View;

class AllComposer {

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $categories = Category::getList();
        $view->with('user',$user);
        $view->with('categories',$categories);
    }
}
