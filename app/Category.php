<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = [
        'id',
        'slug',
        'name',
    ];

    public static function getList($params = [])
    {
        $query = DB::table('categories');
        $query->select(['categories.*',]);
        $list = $query->get();

        return $list;
    }
}
