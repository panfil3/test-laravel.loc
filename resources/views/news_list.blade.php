@extends('index')

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main border-bottom">

                @if (session('subscribed'))
                <div class="mb-3 d-flex align-items-center justify-content-between">
                    <div class="col-12 alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('subscribed') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                <div class="mb-3 d-flex align-items-center justify-content-between">
                    <div>
                        Sort by
                        <a href="{{route('news-by-category', [$cat_slug, 'sort_dir' => $sortDirLink])}}" class="font-weight-bold">Views </a>
                    </div>
                    @if(Auth::check() && empty($subscription))
                        <div>
                            <form action="{{route('subscribe', [$user->id, $cat_id])}}" method="POST" class="ml-3 align-self-end">
                                @csrf
                                <button class="btn btn-sm btn-info" type="submit">Subscribe</button>
                            </form>
                        </div>
                    @elseif (Auth::check() && !empty($subscription))
                        <div>
                            <a href="#" class="btn btn-sm btn-outline-info disabled">Subscribed</a>
                        </div>
                    @endif
                </div>
                @foreach($news as $i => $v)
                <div class="blog-post">
                    <h2 class="blog-post-title">
                        <a href="{{route('news', [$v->cat_slug, $v->slug])}}" >{{$v->title}}</a>
                    </h2>
                    <div class="d-flex align-items-center justify-content-start my-3">
                        <p class="blog-post-meta mb-0">{{date('F d\, Y', strtotime($v->created_at))}} | Views {{$v->views}}</p>
                        @if(Auth::check() && $v->favorites == 0)
                            <form action="{{route('add-to-favorites', [$user->id, $v->id])}}" method="POST" class="ml-3">
                                @csrf
                                <button class="btn btn-sm btn-warning" type="submit">Add to favorites</button>
                            </form>
                        @elseif (Auth::check())
                            <a href="#" class="btn btn-sm btn-outline-warning ml-3 disabled">Added to favorites</a>
                        @endif
                    </div>
                    <p class="mt-3">{{$v->description}}</p>
                </div>
                @endforeach
            </div>

            @include('paginator')

        </div>
    </main>
@endsection
