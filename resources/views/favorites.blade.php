@extends('index')

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main border-bottom">
                @foreach($news as $i => $v)
                <div class="blog-post">
                    <h2 class="blog-post-title">
                        <a href="{{route('news', [$v->cat_slug, $v->slug])}}" >{{$v->title}}</a>
                    </h2>
                    <p class="blog-post-meta">
                        {{date('F d\, Y', strtotime($v->created_at))}} | Views {{$v->views}} | Category: <a href="{{route('news-by-category', [$v->cat_slug])}}">{{$v->category}}</a>
                    </p>
                    <p>{{$v->description}}</p>
                </div>
                @endforeach
            </div>

            @include('paginator')


        </div>
    </main>
@endsection
