<div class="col-md-12 paginator mt-3">
    <nav>
        <ul class="pagination pagination-md row justify-content-center">
            {{ $news->appends(Request::capture()->except('page'))->links() }}
        </ul>
    </nav>
</div>
