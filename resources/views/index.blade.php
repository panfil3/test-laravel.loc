<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('head')
    <body>
        @include('header')
        @yield('content')
    </body>
</html>
