@extends('index')

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main border-bottom">
                <div class="mb-3">
                    Sort by
                    <a href="{{route('home', ['sort_by' => 'views', 'sort_dir' => $sortDirLink])}}" class="font-weight-bold"> Views </a> |
                    <a href="{{route('home', ['sort_by' => 'categories', 'sort_dir' => $sortDirLink])}}" class="font-weight-bold"> Categories </a>
                </div>
                @foreach($news as $i => $v)
                <div class="blog-post">
                    <h2 class="blog-post-title">
                        <a href="{{route('news', [$v->cat_slug, $v->slug])}}" >{{$v->title}}</a>
                    </h2>
                    <div class="d-flex align-items-center justify-content-start my-3">
                        <p class="blog-post-meta mb-0">
                            {{date('F d\, Y', strtotime($v->created_at))}} |
                            Views {{$v->views}} |
                            Category: <a href="{{route('news-by-category', [$v->cat_slug])}}">{{$v->category}}</a>
                        </p>
                        @if(Auth::check() && $v->favorites == 0)
                        <form action="{{route('add-to-favorites', [$user->id, $v->id])}}" method="POST" class="ml-3">
                            @csrf
                            <button class="btn btn-sm btn-warning" type="submit">Add to favorites</button>
                        </form>
                        @elseif (Auth::check())
                            <a href="#" class="btn btn-sm btn-outline-warning ml-3 disabled">Added to favorites</a>
                        @endif
                    </div>
                    <p class="mt-3">{{$v->description}}</p>
                </div>
                @endforeach
            </div>

            @include('paginator')


        </div>
    </main>
@endsection
