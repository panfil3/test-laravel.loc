<div class="container">
    <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-3">
                <a class="blog-header-logo text-dark" href="{{route('home')}}">Test laravel</a>
            </div>
            <div class="col-5 d-flex justify-content-end align-items-center">
                <form class="form-inline mt-2 mt-md-0 mr-2" method="GET" action="{{route('search-result')}}">
                    <input class="form-control form-control-sm mr-sm-2 col-md-8" name="query" type="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-sm btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form>
                @if(!Auth::check())
                    <a class="btn btn-sm btn-success mr-2" href="{{route('login')}}">Sign in</a>
                    <a class="btn btn-sm btn-outline-secondary" href="{{route('register')}}">Sign up</a>
                @else
                    <a class="btn btn-sm btn-warning mr-2" href="{{route('favorites', [$user->id])}}">Favorites</a>
                    <span>
                        <form action="{{route('logout')}}" method="post" name="logout">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-sm btn-danger">Logout </button>
                        </form>
                    </span>
                @endif
            </div>
        </div>
    </header>
    <div class="nav-scroller my-2">
        <nav class="nav">
            @foreach($categories as $category)
                <a class="btn btn-sm btn-secondary mt-2 mr-3" href="{{route('news-by-category', [$category->slug])}}">{{$category->name}}</a>
            @endforeach
        </nav>
    </div>
</div>
