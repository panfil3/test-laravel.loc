@extends('index')

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main border-bottom">
                <div class="blog-post">
                    <h2 class="blog-post-title">{{$news->news_title}}</h2>
                    <p class="blog-post-meta">
                        {{date('F d\, Y', strtotime($news->news_created_at))}} | Views {{$news->news_views}} | Category: <a href="{{route('news-by-category', [$news->cat_slug])}}">{{$news->cat_name}}</a>
                    </p>
                    <p>{{$news->news_description}}</p>
                </div>
            </div>
        </div>
    </main>
@endsection
