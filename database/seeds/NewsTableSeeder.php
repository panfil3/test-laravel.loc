<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert(
            [
                [
                    'id' => 1,
                    'slug' => Str::slug('Santa Clarita shooting'),
                    'title' => 'Santa Clarita shooting',
                    'description' => 'Two Southern California teenagers shot by a 16-year-old classmate are expected to be discharged soon from the hospital, doctors said Friday. The suspect fatally shot two classmates and injured three others before shooting himself in the head with his last bullet during the 16-second attack Thursday morning, said Los Angeles County Sheriff Alex Villanueva. Hospital officials said Friday that the suspect remains in critical condition.Students who recently filmed an active shooting training video said they couldn\'t believe violence struck at Saugus High School in Santa Clarita, home to Six Flags Magic Mountain and 30 miles northwest of downtown Los Angeles. ',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 2,
                    'slug' => Str::slug('Pierce Brosnan\'s sons to be next Golden Globe Awards ambassadors'),
                    'title' => 'Pierce Brosnan\'s sons to be next Golden Globe Awards ambassadors',
                    'description' => 'Pierce Brosnan’s sons have been chosen as the Golden Globe ambassadors to assist with the glitzy awards ceremony. The Hollywood Foreign Press Association announced Thursday evening that 22-year-old Dylan and 18-year-old Paris Brosnan will assume the ambassador roles for the 77th annual Golden Globes Awards in January. An ambassador is traditionally the child of a celebrity who assists with award presentations, handing out trophies to winners and escorting them off stage. The Brosnan brothers said they are more excited than nervous to appear on stage at the awards. “We watched the Globes growing up ... and I’ve gone with him a few times. So it’s truly an honor to be here in this capacity and pay homage to our father’s legacy,” said Dylan Brosnan about his father, a two-time Golden Globe nominee.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 3,
                    'slug' => Str::slug('Rapper Kodak Black sentenced to more than 3 years in weapons case'),
                    'title' => 'Rapper Kodak Black sentenced to more than 3 years in weapons case',
                    'description' => 'Rapper Kodak Black was sentenced Wednesday to more than three years in federal prison after pleading guilty to weapons charges stemming from his arrest just before a scheduled concert performance in May. The 22-year-old Black admitted in August that he falsified information on federal forms to buy four firearms from a Miami-area gun shop on two separate occasions. Black was able to obtain three of the weapons: a 9 mm handgun, a .380-caliber handgun and a semi-automatic Mini Draco weapon. Authorities said one of the guns was found at the scene of a March shooting in Pompano Beach, although he has not been charged in that case. Black also faces drug, weapons and sexual assault charges in other states that remain pending. He has had several previous arrests. U.S. District Judge Federico Moreno could have sentenced Black to the maximum of 10 years, and prosecutors wanted eight years in part because Black allegedly was involved in a jail fight that injured a corrections officer.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 4,
                    'slug' => Str::slug('Netflix Creating Animated Elvis Spy Comedy Series, ‘Agent King’'),
                    'title' => 'Netflix Creating Animated Elvis Spy Comedy Series, ‘Agent King’',
                    'description' => 'Elvis Presley\'s influence was enormously important to both rock and country music, but the iconic singer suffered through a humiliation at the Grand Ole Opry early in his career. Before Presley was considered the King of Rock and Roll, he was called the King of Western Bop. His early records drew a lot from rockabilly for their sound -- a style of music that was essentially a souped-up form of bluegrass fused with a more aggressive rhythm track.Elvis took that style of music to the Opry on Oct. 2, 1954, but far from revolutionizing the music world with his hip-shaking performance and dynamic new sound, the King bombed so badly at his first Opry show that after hearing his rendition of "Blue Moon of Kentucky," Opry talent manager Jim Denny reportedly told him he should go back to his day job as a truck driver.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 5,
                    'slug' => Str::slug('Lori Loughlin\'s firing upset \'Fuller House\' co-star Andrea Barber'),
                    'title' => 'Lori Loughlin\'s firing upset \'Fuller House\' co-star Andrea Barber',
                    'description' => '“Fuller House” star Andrea Barber is opening up about Lori Loughlin’s absence from the Netflix series. In a new interview, Barber addressed the embattled actress and noted that Loughlin’s firing left “a hole on our set.” “It’s very sad. She was a big part of \'Fuller House.\' She wasn’t in every episode, but her presence was definitely felt. We loved every single time she came on the set. So we have definitely felt her absence this season,” Barber, 43, said during an appearance on Us Magazine’s “Watch With Us” podcast. “We wish it wasn’t that way, and we wish she could be here because it definitely feels like there’s a hole in our hearts and a hole on our set,” Barber said. “But we understand that this is the decision that was made, and we respect it. But she’s always in our hearts forever. Every time we step on stage, she’s in our hearts.”',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 6,
                    'slug' => Str::slug('Meghan Markle accused of copying Vogue cover from 2016 book'),
                    'title' => 'Meghan Markle accused of copying Vogue cover from 2016 book',
                    'description' => 'Meghan Markle\'s guest-editing position for the September issue of Vogue U.K. is already making headlines for all of the wrong reasons. Shortly after the issue was released, fans pointed out the glaring similarities the magazine’s cover seemed to share with Australian authors Samantha Brett and Steph Adams’ bestselling book, “The Game Changers."  Brett and Adams told us the similarities between their book, which Meghan contributed to in 2016, is "flattering" but "disappointing."  "It\'s obviously very flattering, she must like our concept! I love Meghan and am a huge fan, but it is a little bit disappointing,” Brett, a columnist in the land down under, told us of the cover choice which shows the faces of various women considered “Forces for Change” and placed in separate boxes.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 7,
                    'slug' => Str::slug('New York City man stabbed, pet dog slashed in subway station, police say'),
                    'title' => 'New York City man stabbed, pet dog slashed in subway station, police say',
                    'description' => 'A homeless man was stabbed and his dog was slashed at a New York City subway station on Thursday morning, according to police. The duo was inside the 14th Street Station at Sixth Avenue in the Chelsea neighborhood of Manhattan just after 4 a.m. when a fight broke out between the 34-year-old, who was sleeping on a bench, and another man.  Investigators said the fight escalated and the suspect stabbed the homeless man in the calf before slashing his pet Rottweiler in the leg with a knife, WABC-TV reported. The suspect fled the subway station after the attack, authorities said. He was said to be a black man in his 30s, wearing a brown jacket, black pants and a black hat. The NYPD said a woman was with him during the incident.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 8,
                    'slug' => Str::slug('Missouri police make grim discovery in a freezer'),
                    'title' => 'Missouri police make grim discovery in a freezer',
                    'description' => 'Police canvassing a Missouri neighborhood on Wednesday made the ultimate "chilling" discovery: a freezer packed with a dead man\'s remains. Investigators from the Joplin Police Department were looking into an arson and burglary that took place on Nov. 5 when they received a tip that a deceased person was in a residence just a few doors away, a spokesman for the department told Fox News. Police executed a search warrant for the residence and found an adult male in the freezer. An autopsy is being scheduled and his identification is being withheld as authorities contact the next of kin, police said, adding that the investigation is ongoing.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 9,
                    'slug' => Str::slug('\'Mysterious ghost ship\' discovered in Lake Michigan'),
                    'title' => '\'Mysterious ghost ship\' discovered in Lake Michigan',
                    'description' => 'The remarkably intact wreck of a schooner that sank in 1891 has been discovered in Lake Michigan. Shipwreck hunter Ross Richardson was traveling to South Manitou Island in northern Lake Michigan when his sonar picked up “something interesting” on the lakebed. Richardson made a record of the GPS coordinates and returned about a week later for a closer inspection. “The sonar showed something rising 90 feet off the bottom, which is very unusual,” he said on his website. Resting in 300 feet of water, the mysterious object was beyond Richardson’s diving range so he called in his friend Steve Wimer, a diver and underwater photographer, to examine the wreck site. On Sept. 30, Richardson, Wimer and their friend Brent Tompkins returned to the site and Wimer swam down to photograph what the website describes as a “mysterious ghost ship.” ',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 10,
                    'slug' => Str::slug('Delta CEO\'s plan to improve airports across the US'),
                    'title' => 'Delta CEO\'s plan to improve airports across the US',
                    'description' => 'Delta Air Lines CEO Ed Bastian spoke with FOX Business\' Gerri Willis about his airline\'s plan to expand across the country and commented on the contentious grilling of Boeing CEO Dennis Muilenburg on Capitol Hill on Tuesday. Delta is spending $3.3 billion to expand its facility at New York\'s LaGuardia Airport and the first phase of that expansion opened Tuesday. "It\'s a beautiful new facility," Bastian told Willis. "Delta, 50 years ago, started flying into LaGuardia Airport with six DC-3s. And today, we have 275 flights a day at LaGuardia -- by far the largest carrier at LaGuardia. And, as a result of that, we need to invest in the infrastructure."  New York isn\'t the only airport getting a Delta facelift, though. Why all this renovation now? Bastian said the aviation industry was in disrepair a decade ago as many airlines were restructuring. But now, airlines are able to compete on quality and service rather than which is the most affordable, he said.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 11,
                    'slug' => Str::slug('Entrepreneur says Democrat tax plans may force him to move out of the country'),
                    'title' => 'Entrepreneur says Democrat tax plans may force him to move out of the country',
                    'description' => 'The holy grail of every entrepreneur is taking risks in order to make more time and money, according to the CEO of Cardone Capital, Grant Cardone. Cardone, the author of "The 10X Rule," creator of multiple business programs and an entrepreneur who owns and operates seven privately held companies, told FOX Business’ Maria Bartiromo that he is concerned that the Democrats’ multitude of tax proposals would stifle business. “I would move out of the country,” he said on Wednesday. It wouldn’t be the first time Cardone moved -- he said the "socialist influence" in California forced him to relocate his businesses to Miami where he was able to grow his company 50 times because the tax bill was 13 percent cheaper. As a result, Cardone said he was able to create more jobs and increase wages.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 12,
                    'slug' => Str::slug('Scarlett Johansson says she was typecast early in her career: \'I was very hyper-sexualized\''),
                    'title' => 'Scarlett Johansson says she was typecast early in her career: \'I was very hyper-sexualized\'',
                    'description' => 'Scarlett Johansson\'s rise to superstardom had a bit of a rocky start. Speaking with The Hollywood Reporter, Johansson, 34, said that she was hypersexualized in her early years. “I feel when I was working in my early 20s and even in my late teens/early 20s, I felt that I sort of got, somehow, typecast. I was very hyper-sexualized," she admitted. "Which, I guess, at the time seemed OK to everyone. It was another time.” Johansson\'s film career began in 1994 with "North" and quickly blossomed from there. One of her most notable roles at the time was in the film "Lost in Translation," in which, her character catches the eye of Bill Murray\'s middle-aged character. She was 18 at the time of the film\'s release. Johansson was also lauded for the 2003 film "Girl with a Pearl Earring" for playing the muse of Colin Firth\'s character. The two actors have a 25-year age difference. Both performances would earn her Golden Globe nominations.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 13,
                    'slug' => Str::slug('Christina Hendricks steps out for first red carpet appearance since announcing split from husband'),
                    'title' => 'Christina Hendricks steps out for first red carpet appearance since announcing split from husband',
                    'description' => 'Actress Christina Hendricks is ready for a fresh start. The "Mad Men" star, 44, stepped out for NBC and Vanity Fair\'s "Celebration of the Season" party in Los Angeles on Monday. She donned a teal dress with black straps for the occasion. Hendricks announced the separation from her husband, Geoffrey Arend, in mid-October after 10 years of marriage. "Today we take our next step together, but on separate paths," Hendricks and Arend, 41, said in a message that they both shared to Instagram. "We will always be grateful for the love we\'ve shared and will always work together to raise our two beautiful dogs." The statement went on to thank fans for their support, patience and space as the two actors "rediscover" themselves.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 14,
                    'slug' => Str::slug('Pedestrians looting fish during overturned truck accident goes viral'),
                    'title' => 'Pedestrians looting fish during overturned truck accident goes viral',
                    'description' => 'A video of people looting live fish strewn across a busy street in Kapur, India, after a truck accident has gone viral. The video, posted to Twitter on Tuesday, shows a massive crowd of people picking up the fish after a truck overturned in the middle of the road. The live, squirming fish can be seen lying all over the street during the chaotic scene, as pedestrians grabbed as many of them as they could. “Not a loot. Disaster management workers rescuing the victims!” a Twitter user commented. “It seems that today a dish of fish will be made in Kapur,” another person tweeted. Another Twitter user appeared to chastise the people for the looting of the “suffering” fish, writing: “How happy is the public to be angry at the suffering creature.”  The incident came just a month after another truck full of poultry crashed in Odisha, India, according to News18.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 15,
                    'slug' => Str::slug('Texas fisherman lands 200-pound alligator gar after 40-minute tussle'),
                    'title' => 'Texas fisherman lands 200-pound alligator gar after 40-minute tussle',
                    'description' => 'A kayak fisherman nearly lost his balance while reeling in a giant 200-pound alligator gar in Texas last week. Chris Hernandez was fishing on a river on the outskirts of San Benito, bobbing along in his kayak, when he hooked the monstrous creature and a 40-minute struggle ensued. "He was dragging me all over the river," Hernandez said to My San Antonio. "For a minute I thought I was going to lose my pole because it was like halfway in the water." Hernandez said that during the tussle, the seven-foot gar -- the beasties are known for large, sharp teeth used to impale and hold prey -- tried to flip over his kayak using its tail. Eventually, though, Hernandez managed to reel in the fish, though that presented an entirely new set of challenges. The fish was too large to fit on his kayak. According to the local outlet, Hernandez held his fishing pole in one hand and rowed with the other to bring his kayak and catch back to shore.',
                    'views' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]
        );
    }
}
